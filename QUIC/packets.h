/*
 * packets.h
 *
 *  Created on: Nov 7, 2017
 *      Author: anay
 */

#ifndef PACKETS_H_
#define PACKETS_H_

#include <arpa/inet.h>
#include <stdlib.h>
#include <stdint.h>

typedef std::bitset<7> check;
typedef std::bitset<5> check2;

struct LH {
    bool lh;
    check type;
    int64_t CID;
    int32_t PN;
    int32_t version;
};

struct SH {
    bool lh;
    bool C;
    bool K;
    check2 type;
    int64_t CID;
    int32_t PN;
};



#endif /* PACKETS_H_ */
