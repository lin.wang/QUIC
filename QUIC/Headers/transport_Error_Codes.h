/*
 * error_Codes.h
 *
 *  Created on: Nov 27, 2017
 *      Author: anay
 */

#ifndef HEADERS_TRANSPORT_ERROR_CODES_H_
#define HEADERS_TRANSPORT_ERROR_CODES_H_

#define NO_ERROR 0x00
#define INTERNAL_ERROR 0x01
#define FLOW_CONTROL_ERROR 0x03
#define STREAM_ID_ERROR 0x04
#define STREAM_STATE_ERROR 0x05
#define FINAL_OFFSET_ERROR 0x06
#define FRAME_FORMAT_ERROR 0x07
#define TRANSPORT_PARAMETER_ERROR 0x08
#define VERSION_NEGOTIATION_ERROR 0x09
#define PROTOCOL_VIOLATION 0x0A
#define PADDING_FRAME_ERROR 0x100
#define RST_STREAM_ERROR 0x101
#define CONNECTION_CLOSE_FRAME_ERROR 0x102
#define APPLICATION_CLOSE_FRAME_ERROR 0x103
#define MAX_DATA_FRAME_ERROR 0x104
#define MAX_STREAM_DATA_FRAME_ERROR 0x105
#define MAX_STREAM_ID_FRAME_ERROR 0x106
#define PING_FRAME_ERROR 0x107
#define BLOCKED_FRAME_ERROR 0x108
#define STREAM_BLOCKED_FRAME_ERROR 0x109
#define STREAM_ID_BLOCKED_FRAME_ERROR 0x10A
#define NEW_CONNECTION_ID_FRAME_ERROR 0x10B
#define STOP_SENDING_FRAME_ERROR 0x10C


#endif /* HEADERS_TRANSPORT_ERROR_CODES_H_ */
