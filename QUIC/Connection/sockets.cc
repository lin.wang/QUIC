/*
 * socket.c
 *
 *  Created on: Nov 27, 2017
 *      Author: anay
 */

#include "Connection/sockets.h"

Define_Module(send_UDP);

using namespace inet;

void send_UDP::send_packet (cPacket *msg, L3Address remoteAddr, int remotePort, UDPSocket socket) {
    socket.connect(remoteAddr, remotePort);
    socket.send(msg);
}

UDPSocket send_UDP::create_UDPSocket(L3Address localAddr, int localPort) {
    UDPSocket socket;
    socket.setOutputGate(gate("udpOut"));
    socket.bind(localAddr, localPort);
    return socket;
}

void send_UDP::close_UDPSocket(UDPSocket socket) {
    socket.close();
}

void send_UDP::receive_packet(UDPSocket socket, cMessage* msg){

}
