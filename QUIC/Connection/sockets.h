/*
 * sockets.h
 *
 *  Created on: Nov 27, 2017
 *      Author: anay
 */

#ifndef CONNECTION_SOCKETS_H_
#define CONNECTION_SOCKETS_H_

#include <omnetpp.h>
#include <netinet/in.h>
#include "/home/anay/Documents/omnetpp-5.2/samples/inet/src/inet/transportlayer/contract/udp/UDPSocket.h"
#include "/home/anay/Documents/omnetpp-5.2/samples/inet/src/inet/networklayer/common/L3AddressResolver.h"
#include "/home/anay/Documents/omnetpp-5.2/samples/inet/src/inet/transportlayer/udp/UDPPacket.h"

using namespace inet;

class send_UDP : public cSimpleModule {
public:
    void send_packet (cPacket *msg, L3Address remoteAddr, int remotePort, UDPSocket socket) ;
    UDPSocket create_UDPSocket(L3Address localAddr, int localPort);
    void close_UDPSocket(UDPSocket socket);
    void receive_packet (UDPSocket socket, cMessage *msg);
};

#endif /* CONNECTION_SOCKETS_H_ */
