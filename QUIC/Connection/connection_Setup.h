/*
 * connection_Setup.h
 *
 *  Created on: Nov 27, 2017
 *      Author: anay
 */

#ifndef CONNECTION_CONNECTION_SETUP_H_
#define CONNECTION_CONNECTION_SETUP_H_

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <omnetpp.h>
#include "Connection/connection.h"
#include "Connection/sockets.h"

class connection_Setup: public send_UDP {

private:
    UDPSocket socket;
    uint64_t CID;
    Long_Header *pkt;
    uint32_t PN = 1;
    uint32_t version;
    uint32_t lastAckPn;
public:
    void initialize(int stage);
    void handleMessage(cMessage *msg);
    void sendVersionNegotiation();
    void receiveVersionNegotiation(Long_Header *pkt);
    void sendClientInitial(uint32_t version, const char* string);
    void receiveClientInitial(Long_Header *pkt);
    void sendClientCleartext(uint32_t version, const char* string);
    void receiveClientCleartext(Long_Header *pkt);
    void sendServerStatelessRetry(uint32_t version, const char* string);
    void receiveServerStatelessRetry(Long_Header *pkt);
    void sendServerCleartext(uint32_t version, const char* string);
    void receiveServerCleartext(Long_Header *pkt);
    void sendRTTProtected(uint32_t version, const char* string);
    void receiveRTTProtected(Long_Header *pkt);

};

#endif /* CONNECTION_CONNECTION_SETUP_H_ */
