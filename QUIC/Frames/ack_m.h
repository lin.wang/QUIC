//
// Generated file, do not edit! Created by nedtool 5.2 from Frames/ack.msg.
//

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wreserved-id-macro"
#endif
#ifndef __ACK_M_H
#define __ACK_M_H

#include <omnetpp.h>

// nedtool version check
#define MSGC_VERSION 0x0502
#if (MSGC_VERSION!=OMNETPP_VERSION)
#    error Version mismatch! Probably this file was generated by an earlier version of nedtool: 'make clean' should help.
#endif



/**
 * Class generated from <tt>Frames/ack.msg:19</tt> by nedtool.
 * <pre>
 * //
 * // TODO generated message class
 * //
 * class Ack
 * {
 *     uint8_t type;
 *     uint8_t num_Blocks;
 *     uint32_t largest_Acknowledged;
 *     uint16_t ack_Delay;
 *     string ack_Block_Section;
 * }
 * </pre>
 */
class Ack : public ::omnetpp::cObject
{
  protected:
    uint8_t type;
    uint8_t num_Blocks;
    uint32_t largest_Acknowledged;
    uint16_t ack_Delay;
    ::omnetpp::opp_string ack_Block_Section;

  private:
    void copy(const Ack& other);

  protected:
    // protected and unimplemented operator==(), to prevent accidental usage
    bool operator==(const Ack&);

  public:
    Ack();
    Ack(const Ack& other);
    virtual ~Ack();
    Ack& operator=(const Ack& other);
    virtual Ack *dup() const override {return new Ack(*this);}
    virtual void parsimPack(omnetpp::cCommBuffer *b) const override;
    virtual void parsimUnpack(omnetpp::cCommBuffer *b) override;

    // field getter/setter methods
    virtual uint8_t getType() const;
    virtual void setType(uint8_t type);
    virtual uint8_t getNum_Blocks() const;
    virtual void setNum_Blocks(uint8_t num_Blocks);
    virtual uint32_t getLargest_Acknowledged() const;
    virtual void setLargest_Acknowledged(uint32_t largest_Acknowledged);
    virtual uint16_t getAck_Delay() const;
    virtual void setAck_Delay(uint16_t ack_Delay);
    virtual const char * getAck_Block_Section() const;
    virtual void setAck_Block_Section(const char * ack_Block_Section);
};

inline void doParsimPacking(omnetpp::cCommBuffer *b, const Ack& obj) {obj.parsimPack(b);}
inline void doParsimUnpacking(omnetpp::cCommBuffer *b, Ack& obj) {obj.parsimUnpack(b);}


#endif // ifndef __ACK_M_H

