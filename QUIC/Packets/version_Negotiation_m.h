//
// Generated file, do not edit! Created by nedtool 5.2 from Packets/version_Negotiation.msg.
//

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wreserved-id-macro"
#endif
#ifndef __VERSION_NEGOTIATION_M_H
#define __VERSION_NEGOTIATION_M_H

#include <omnetpp.h>

// nedtool version check
#define MSGC_VERSION 0x0502
#if (MSGC_VERSION!=OMNETPP_VERSION)
#    error Version mismatch! Probably this file was generated by an earlier version of nedtool: 'make clean' should help.
#endif



/**
 * Class generated from <tt>Packets/version_Negotiation.msg:19</tt> by nedtool.
 * <pre>
 * //
 * // TODO generated message class
 * //
 * packet Version_Negotiation
 * {
 *     string versions;
 * }
 * </pre>
 */
class Version_Negotiation : public ::omnetpp::cPacket
{
  protected:
    ::omnetpp::opp_string versions;

  private:
    void copy(const Version_Negotiation& other);

  protected:
    // protected and unimplemented operator==(), to prevent accidental usage
    bool operator==(const Version_Negotiation&);

  public:
    Version_Negotiation(const char *name=nullptr, short kind=0);
    Version_Negotiation(const Version_Negotiation& other);
    virtual ~Version_Negotiation();
    Version_Negotiation& operator=(const Version_Negotiation& other);
    virtual Version_Negotiation *dup() const override {return new Version_Negotiation(*this);}
    virtual void parsimPack(omnetpp::cCommBuffer *b) const override;
    virtual void parsimUnpack(omnetpp::cCommBuffer *b) override;

    // field getter/setter methods
    virtual const char * getVersions() const;
    virtual void setVersions(const char * versions);
};

inline void doParsimPacking(omnetpp::cCommBuffer *b, const Version_Negotiation& obj) {obj.parsimPack(b);}
inline void doParsimUnpacking(omnetpp::cCommBuffer *b, Version_Negotiation& obj) {obj.parsimUnpack(b);}


#endif // ifndef __VERSION_NEGOTIATION_M_H

