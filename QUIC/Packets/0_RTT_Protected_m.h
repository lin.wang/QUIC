//
// Generated file, do not edit! Created by nedtool 5.2 from Packets/0_RTT_Protected.msg.
//

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wreserved-id-macro"
#endif
#ifndef __0_RTT_PROTECTED_M_H
#define __0_RTT_PROTECTED_M_H

#include <omnetpp.h>

// nedtool version check
#define MSGC_VERSION 0x0502
#if (MSGC_VERSION!=OMNETPP_VERSION)
#    error Version mismatch! Probably this file was generated by an earlier version of nedtool: 'make clean' should help.
#endif



// cplusplus {{
#include "Frames/stream_m.h"
typedef Stream* Streamptr;
// }}

/**
 * Class generated from <tt>Packets/0_RTT_Protected.msg:25</tt> by nedtool.
 * <pre>
 * packet _0_RTT_Protected
 * {
 *     Streamptr stream_0;
 * }
 * </pre>
 */
class _0_RTT_Protected : public ::omnetpp::cPacket
{
  protected:
    Streamptr stream_0;

  private:
    void copy(const _0_RTT_Protected& other);

  protected:
    // protected and unimplemented operator==(), to prevent accidental usage
    bool operator==(const _0_RTT_Protected&);

  public:
    _0_RTT_Protected(const char *name=nullptr, short kind=0);
    _0_RTT_Protected(const _0_RTT_Protected& other);
    virtual ~_0_RTT_Protected();
    _0_RTT_Protected& operator=(const _0_RTT_Protected& other);
    virtual _0_RTT_Protected *dup() const override {return new _0_RTT_Protected(*this);}
    virtual void parsimPack(omnetpp::cCommBuffer *b) const override;
    virtual void parsimUnpack(omnetpp::cCommBuffer *b) override;

    // field getter/setter methods
    virtual Streamptr& getStream_0();
    virtual const Streamptr& getStream_0() const {return const_cast<_0_RTT_Protected*>(this)->getStream_0();}
    virtual void setStream_0(const Streamptr& stream_0);
};

inline void doParsimPacking(omnetpp::cCommBuffer *b, const _0_RTT_Protected& obj) {obj.parsimPack(b);}
inline void doParsimUnpacking(omnetpp::cCommBuffer *b, _0_RTT_Protected& obj) {obj.parsimUnpack(b);}


#endif // ifndef __0_RTT_PROTECTED_M_H

